package com.devcamp.jbr330.jbr330;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {
    @CrossOrigin
    @GetMapping("/employees")
    public ArrayList<Employee> getListEmployees() {
        ArrayList<Employee> listemp = new ArrayList<Employee>();

        Employee employee1 = new Employee(1, "Quan", "Nguyen", 15000);
        Employee employee2 = new Employee(2, "Boi", "Ho", 13000);
        Employee employee3 = new Employee(3, "Thu", "Nguyen", 10000);
        
        listemp.add(employee1);
        listemp.add(employee2);
        listemp.add(employee3);

        return listemp;
    }

    // public static void main(String[] args) {
    //     Employee employee1 = new Employee(1, "Quan", "Nguyen", 15000);
    //     Employee employee2 = new Employee(2, "Boi", "Ho", 13000);
    //     Employee employee3 = new Employee(3, "Thu", "Nguyen", 10000);
    //     System.out.println(employee1 + "," + employee2 + "," + employee3);
    // }
    
}
